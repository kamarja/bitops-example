# Test EKS 

describe eks('qa-bitops') do
  it { should exist }
end

be_active, be_creating
describe eks('qa-bitops') do
  it { should be_active }
end

# ELB resource type.

exist
describe elb('bitops-elb') do
  it { should exist }
end

describe elb('bitops-elb') do
  it { should have_listener(protocol: 'HTTPS', port: 443, instance_protocol: 'HTTP', instance_port: 80) }
end

# have_tag

describe elb('bitops-elb') do
  it { should have_tag('Name').value('bitops-elb') }
  it { should have_tag('bitops-tag-key').value('bitops-tag-value') }
end

# have_custom_response_error_code
describe cloudfront_distribution('123456789zyxw.cloudfront.net') do
  it do
    should have_custom_response_error_code(400)
      .error_caching_min_ttl(60)
      .response_page_path('/path/to/400.html')
      .response_code(400)
  end
  it do
    should have_custom_response_error_code(403)
      .error_caching_min_ttl(60)
      .response_page_path('/path/to/403.html')
      .response_code('403')
  end
  it do
    should have_custom_response_error_code(500)
      .error_caching_min_ttl(60)
  end
end

# have_origin_domain_name_and_path
describe cloudfront_distribution('123456789zyxw.cloudfront.net') do
  it { should have_origin_domain_name_and_path('cf-s3-origin-hosting.dev.bitovidevops.com.s3.amazonaws.com/img') }
end

EC2 resource type.

exist
describe ec2('bitops-ec2') do
  it { should exist }
end
be_disabled_api_termination
describe ec2('bitops-ec2') do
  it { should be_disabled_api_termination }
end

be_pending, be_running, be_shutting_down, be_terminated, be_stopping, be_stopped
describe ec2('bitops-ec2') do
  it { should be_running }
end

belong_to_vpc
describe ec2('bitops-ec2') do
  it { should belong_to_vpc('vpc-ab123cde') }
  it { should belong_to_vpc('bitops-vpc') }
end

describe ec2('bitops-ec2') do
  its('vpc.id') { should eq 'vpc-ab123cde' }
end
or

describe ec2('bitops-ec2') do
  its('resource.vpc.id') { should eq 'vpc-ab123cde' }
end

# route53_hosted_zone
# Route53HostedZone resource type.

describe route53_hosted_zone('bitovidevops.com.') do
  it { should exist }
end

